package filmes;

import java.util.HashMap;
import java.util.Map;

public class EvalVisitor extends FilmesBaseVisitor<Object> {
    
    private final Map<String, Map<String, Integer>> memory = new HashMap<>();
    private String movie = "";
    private int maxDuration = -1;
    
    public void printResults() {
        this.memory.forEach((produtor, map) -> {
            System.out.println(produtor + " - " + map.get("total") + "\n"
                    + "C: " + map.get("comedia") + "\n"
                    + "R: " + map.get("romance") + "\n");
        });
        
        System.out.println("Filme com maior duração de 2015: " + this.movie);
    }
    
    private int duracao2minutos(String duration) {
        String[] time = duration.split("\\:");
        
        return Integer.parseInt(time[0]) * 60 + Integer.parseInt(time[1]);
    }
    
    @Override
    public Object visitStart(FilmesParser.StartContext ctx) {
        return visitChildren(ctx);
    }
    
    @Override
    public Object visitGeneros(FilmesParser.GenerosContext ctx) {
        String result = (String) visitChildren(ctx);
        String space = " ";
        
        if (result == null) {
            result = "";
            space = "";
        }
        
        return ctx.GENERO().getText() + space + result;
    }

    @Override
    public Object visitExprs(FilmesParser.ExprsContext ctx) {
        String produtor = ctx.PRODUTOR().getText();
        String titulo = ctx.TITULO().getText();
        String ano = ctx.ANO().getText();
        String duracao = ctx.DURACAO().getText();
        
        int duration = duracao2minutos(duracao);
        
        if (ano.equals("2015") && duration > this.maxDuration) {
            this.maxDuration = duration;
            this.movie = titulo;
        }
        
        int comedia = 0, romance = 0;
        
        if (ctx.generos() != null) {
            String[] generos = ((String) visit(ctx.generos())).split(" ");
            
            for (String genero : generos) {
                if (genero.equals("COMEDIA")) {
                    comedia++;
                }
                
                if (genero.equals("ROMANCE")) {
                    romance++;
                }
            }
        }
        
        if (this.memory.containsKey(produtor)) {
            HashMap<String, Integer> data = (HashMap) this.memory.get(produtor);
            data.put("total", data.get("total") + 1);
            data.put("romance", data.get("romance") + romance);
            data.put("comedia", data.get("comedia") + comedia);
        } else {
            Map<String, Integer> data = new HashMap<>();
            data.put("total", 1);
            data.put("romance", romance);
            data.put("comedia", comedia);
            this.memory.put(produtor, data);
        }
        
        return visitChildren(ctx);
    }
}
