grammar Filmes;

start : exprs+ EOF;
		
exprs : PRODUTOR ' ' TITULO ' ' generos ' ' ANO ' ' DURACAO NEWLINE
        | PRODUTOR ' ' TITULO ' ' generos ' ' ANO ' ' DURACAO 
        | PRODUTOR ' ' TITULO ' ' ANO ' ' DURACAO NEWLINE
        | PRODUTOR ' ' TITULO ' ' ANO ' ' DURACAO
	;
		
generos : GENERO ',' generos
          | GENERO
          ;

PRODUTOR : [A-Z][A-Z][A-Z]'-'[0-9][0-9]'.'[a-zA-Z0-9];
		
TITULO : '"'[A-Za-z: ]+'"';
                    
GENERO : 'ACAO'|'COMEDIA'|'ROMANCE'|'FICCAO'|'TERROR';
 
ANO : '19'[0-9][0-9]|'200'[0-9]|'201'[0-6];

DURACAO : (([0-1][0-9])|('2'[0-3]))':'[0-5][0-9];

NEWLINE : [\r\n]+;

ESPACO : ' ';

VIRGULA : ',';