/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filmes;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

public class Filmes {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream(new File("teste.txt"));

        FilmesLexer lexer = new FilmesLexer(new ANTLRInputStream(fis));
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        FilmesParser parser = new FilmesParser(tokens);
        ParseTree tree = parser.start(); // parse

        EvalVisitor eval = new EvalVisitor();
        eval.visit(tree);
        eval.printResults();
    }
    
}
