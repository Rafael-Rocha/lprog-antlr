// Generated from C:\Users\Rafa\Documents\NetBeansProjects\antlr\lprog-antlr\src\filmes\Filmes.g4 by ANTLR 4.1
package filmes;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class FilmesLexer extends Lexer {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		PRODUTOR=1, TITULO=2, GENERO=3, ANO=4, DURACAO=5, NEWLINE=6, ESPACO=7, 
		VIRGULA=8;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] tokenNames = {
		"<INVALID>",
		"PRODUTOR", "TITULO", "GENERO", "ANO", "DURACAO", "NEWLINE", "' '", "','"
	};
	public static final String[] ruleNames = {
		"PRODUTOR", "TITULO", "GENERO", "ANO", "DURACAO", "NEWLINE", "ESPACO", 
		"VIRGULA"
	};


	public FilmesLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "Filmes.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\uacf5\uee8c\u4f5d\u8b0d\u4a45\u78bd\u1b2f\u3378\2\nh\b\1\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\3\2\3\2\3\2\3\2"+
		"\3\2\3\2\3\2\3\2\3\2\3\3\3\3\6\3\37\n\3\r\3\16\3 \3\3\3\3\3\4\3\4\3\4"+
		"\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3"+
		"\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\5\4C\n\4\3\5\3\5\3\5\3\5\3\5\3"+
		"\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\5\5T\n\5\3\6\3\6\3\6\3\6\5\6Z\n"+
		"\6\3\6\3\6\3\6\3\6\3\7\6\7a\n\7\r\7\16\7b\3\b\3\b\3\t\3\t\2\n\3\3\1\5"+
		"\4\1\7\5\1\t\6\1\13\7\1\r\b\1\17\t\1\21\n\1\3\2\13\3\2C\\\3\2\62;\5\2"+
		"\62;C\\c|\6\2\"\"<<C\\c|\3\2\628\3\2\62\63\3\2\62\65\3\2\62\67\4\2\f\f"+
		"\17\17p\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2"+
		"\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\3\23\3\2\2\2\5\34\3\2\2\2\7B\3\2"+
		"\2\2\tS\3\2\2\2\13Y\3\2\2\2\r`\3\2\2\2\17d\3\2\2\2\21f\3\2\2\2\23\24\t"+
		"\2\2\2\24\25\t\2\2\2\25\26\t\2\2\2\26\27\7/\2\2\27\30\t\3\2\2\30\31\t"+
		"\3\2\2\31\32\7\60\2\2\32\33\t\4\2\2\33\4\3\2\2\2\34\36\7$\2\2\35\37\t"+
		"\5\2\2\36\35\3\2\2\2\37 \3\2\2\2 \36\3\2\2\2 !\3\2\2\2!\"\3\2\2\2\"#\7"+
		"$\2\2#\6\3\2\2\2$%\7C\2\2%&\7E\2\2&\'\7C\2\2\'C\7Q\2\2()\7E\2\2)*\7Q\2"+
		"\2*+\7O\2\2+,\7G\2\2,-\7F\2\2-.\7K\2\2.C\7C\2\2/\60\7T\2\2\60\61\7Q\2"+
		"\2\61\62\7O\2\2\62\63\7C\2\2\63\64\7P\2\2\64\65\7E\2\2\65C\7G\2\2\66\67"+
		"\7H\2\2\678\7K\2\289\7E\2\29:\7E\2\2:;\7C\2\2;C\7Q\2\2<=\7V\2\2=>\7G\2"+
		"\2>?\7T\2\2?@\7T\2\2@A\7Q\2\2AC\7T\2\2B$\3\2\2\2B(\3\2\2\2B/\3\2\2\2B"+
		"\66\3\2\2\2B<\3\2\2\2C\b\3\2\2\2DE\7\63\2\2EF\7;\2\2FG\3\2\2\2GH\t\3\2"+
		"\2HT\t\3\2\2IJ\7\64\2\2JK\7\62\2\2KL\7\62\2\2LM\3\2\2\2MT\t\3\2\2NO\7"+
		"\64\2\2OP\7\62\2\2PQ\7\63\2\2QR\3\2\2\2RT\t\6\2\2SD\3\2\2\2SI\3\2\2\2"+
		"SN\3\2\2\2T\n\3\2\2\2UV\t\7\2\2VZ\t\3\2\2WX\7\64\2\2XZ\t\b\2\2YU\3\2\2"+
		"\2YW\3\2\2\2Z[\3\2\2\2[\\\7<\2\2\\]\t\t\2\2]^\t\3\2\2^\f\3\2\2\2_a\t\n"+
		"\2\2`_\3\2\2\2ab\3\2\2\2b`\3\2\2\2bc\3\2\2\2c\16\3\2\2\2de\7\"\2\2e\20"+
		"\3\2\2\2fg\7.\2\2g\22\3\2\2\2\b\2 BSYb";
	public static final ATN _ATN =
		ATNSimulator.deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}