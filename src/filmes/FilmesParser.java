// Generated from C:\Users\Rafa\Documents\NetBeansProjects\antlr\lprog-antlr\src\filmes\Filmes.g4 by ANTLR 4.1
package filmes;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class FilmesParser extends Parser {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		PRODUTOR=1, TITULO=2, GENERO=3, ANO=4, DURACAO=5, NEWLINE=6, ESPACO=7, 
		VIRGULA=8;
	public static final String[] tokenNames = {
		"<INVALID>", "PRODUTOR", "TITULO", "GENERO", "ANO", "DURACAO", "NEWLINE", 
		"' '", "','"
	};
	public static final int
		RULE_start = 0, RULE_exprs = 1, RULE_generos = 2;
	public static final String[] ruleNames = {
		"start", "exprs", "generos"
	};

	@Override
	public String getGrammarFileName() { return "Filmes.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public FilmesParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class StartContext extends ParserRuleContext {
		public TerminalNode EOF() { return getToken(FilmesParser.EOF, 0); }
		public ExprsContext exprs(int i) {
			return getRuleContext(ExprsContext.class,i);
		}
		public List<ExprsContext> exprs() {
			return getRuleContexts(ExprsContext.class);
		}
		public StartContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_start; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FilmesVisitor ) return ((FilmesVisitor<? extends T>)visitor).visitStart(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StartContext start() throws RecognitionException {
		StartContext _localctx = new StartContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_start);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(7); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(6); exprs();
				}
				}
				setState(9); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==PRODUTOR );
			setState(11); match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprsContext extends ParserRuleContext {
		public GenerosContext generos() {
			return getRuleContext(GenerosContext.class,0);
		}
		public TerminalNode ANO() { return getToken(FilmesParser.ANO, 0); }
		public TerminalNode NEWLINE() { return getToken(FilmesParser.NEWLINE, 0); }
		public TerminalNode DURACAO() { return getToken(FilmesParser.DURACAO, 0); }
		public TerminalNode PRODUTOR() { return getToken(FilmesParser.PRODUTOR, 0); }
		public TerminalNode TITULO() { return getToken(FilmesParser.TITULO, 0); }
		public ExprsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_exprs; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FilmesVisitor ) return ((FilmesVisitor<? extends T>)visitor).visitExprs(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExprsContext exprs() throws RecognitionException {
		ExprsContext _localctx = new ExprsContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_exprs);
		try {
			setState(49);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(13); match(PRODUTOR);
				setState(14); match(ESPACO);
				setState(15); match(TITULO);
				setState(16); match(ESPACO);
				setState(17); generos();
				setState(18); match(ESPACO);
				setState(19); match(ANO);
				setState(20); match(ESPACO);
				setState(21); match(DURACAO);
				setState(22); match(NEWLINE);
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(24); match(PRODUTOR);
				setState(25); match(ESPACO);
				setState(26); match(TITULO);
				setState(27); match(ESPACO);
				setState(28); generos();
				setState(29); match(ESPACO);
				setState(30); match(ANO);
				setState(31); match(ESPACO);
				setState(32); match(DURACAO);
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(34); match(PRODUTOR);
				setState(35); match(ESPACO);
				setState(36); match(TITULO);
				setState(37); match(ESPACO);
				setState(38); match(ANO);
				setState(39); match(ESPACO);
				setState(40); match(DURACAO);
				setState(41); match(NEWLINE);
				}
				break;

			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(42); match(PRODUTOR);
				setState(43); match(ESPACO);
				setState(44); match(TITULO);
				setState(45); match(ESPACO);
				setState(46); match(ANO);
				setState(47); match(ESPACO);
				setState(48); match(DURACAO);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GenerosContext extends ParserRuleContext {
		public GenerosContext generos() {
			return getRuleContext(GenerosContext.class,0);
		}
		public TerminalNode GENERO() { return getToken(FilmesParser.GENERO, 0); }
		public GenerosContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_generos; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FilmesVisitor ) return ((FilmesVisitor<? extends T>)visitor).visitGeneros(this);
			else return visitor.visitChildren(this);
		}
	}

	public final GenerosContext generos() throws RecognitionException {
		GenerosContext _localctx = new GenerosContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_generos);
		try {
			setState(55);
			switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(51); match(GENERO);
				setState(52); match(VIRGULA);
				setState(53); generos();
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(54); match(GENERO);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\uacf5\uee8c\u4f5d\u8b0d\u4a45\u78bd\u1b2f\u3378\3\n<\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\3\2\6\2\n\n\2\r\2\16\2\13\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3\64\n\3\3\4\3\4\3"+
		"\4\3\4\5\4:\n\4\3\4\2\5\2\4\6\2\2=\2\t\3\2\2\2\4\63\3\2\2\2\69\3\2\2\2"+
		"\b\n\5\4\3\2\t\b\3\2\2\2\n\13\3\2\2\2\13\t\3\2\2\2\13\f\3\2\2\2\f\r\3"+
		"\2\2\2\r\16\7\2\2\3\16\3\3\2\2\2\17\20\7\3\2\2\20\21\7\t\2\2\21\22\7\4"+
		"\2\2\22\23\7\t\2\2\23\24\5\6\4\2\24\25\7\t\2\2\25\26\7\6\2\2\26\27\7\t"+
		"\2\2\27\30\7\7\2\2\30\31\7\b\2\2\31\64\3\2\2\2\32\33\7\3\2\2\33\34\7\t"+
		"\2\2\34\35\7\4\2\2\35\36\7\t\2\2\36\37\5\6\4\2\37 \7\t\2\2 !\7\6\2\2!"+
		"\"\7\t\2\2\"#\7\7\2\2#\64\3\2\2\2$%\7\3\2\2%&\7\t\2\2&\'\7\4\2\2\'(\7"+
		"\t\2\2()\7\6\2\2)*\7\t\2\2*+\7\7\2\2+\64\7\b\2\2,-\7\3\2\2-.\7\t\2\2."+
		"/\7\4\2\2/\60\7\t\2\2\60\61\7\6\2\2\61\62\7\t\2\2\62\64\7\7\2\2\63\17"+
		"\3\2\2\2\63\32\3\2\2\2\63$\3\2\2\2\63,\3\2\2\2\64\5\3\2\2\2\65\66\7\5"+
		"\2\2\66\67\7\n\2\2\67:\5\6\4\28:\7\5\2\29\65\3\2\2\298\3\2\2\2:\7\3\2"+
		"\2\2\5\13\639";
	public static final ATN _ATN =
		ATNSimulator.deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}