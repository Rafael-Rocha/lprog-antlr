// Generated from C:\Users\Rafa\Documents\NetBeansProjects\antlr\lprog-antlr\src\filmes\Filmes.g4 by ANTLR 4.1
package filmes;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link FilmesParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface FilmesVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link FilmesParser#start}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStart(@NotNull FilmesParser.StartContext ctx);

	/**
	 * Visit a parse tree produced by {@link FilmesParser#generos}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGeneros(@NotNull FilmesParser.GenerosContext ctx);

	/**
	 * Visit a parse tree produced by {@link FilmesParser#exprs}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprs(@NotNull FilmesParser.ExprsContext ctx);
}